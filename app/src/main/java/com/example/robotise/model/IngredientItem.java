package com.example.robotise.model;

import java.io.Serializable;

/**
 * Ingrédient item représente un ingrédient, il a un nom, une quantité en cl
 */
public class IngredientItem implements Serializable {
    public String name;
    public int quantity;
    public int bec;
    public int id;

    /**
     * Constructeur
     * @param name = nom de l'ingrédient
     * @param quantity = quantité de l'ingrédient
     * @param id = id de l'ingrédient
     *
     */
    public IngredientItem(String name, int quantity, int id){
        this.name = name;
        this.quantity=quantity;
        this.id = id;

    }

    /**
     *
     * @param name : nom de l'ingrédient
     * @param quantity : quantité en cl de l'ingrédient
     * @param bec : bec dans lequel se situe l'ingrédient
     * @param id : id de l'ingrédient
     */
    public IngredientItem(String name, int quantity, int bec, int id){
        this.name = name;
        this.quantity=quantity;
        this.bec = bec;
        this.id = id;

    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Copy the ingredient with a quantity = 0
     * @return
     */
    public IngredientItem copy(){
        return new IngredientItem(this.name, 0, this.id);
    }
}
