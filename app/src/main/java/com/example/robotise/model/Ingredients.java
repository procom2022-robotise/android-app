package com.example.robotise.model;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.robotise.ble.DeviceControl;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Liste des ingrédients que contient la machine
 */
public class Ingredients {
    private static Ingredients instance;
    public ArrayList<IngredientItem> ings;




    /**
     * Créer une liste d'ingrédients par défault
     */
    public Ingredients(){
        instance = this;
        IngredientItem vodka = new IngredientItem("Vodka", 10, 0, 0);

        IngredientItem orange = new IngredientItem("orange", 10, 1, 1);

        ings = new ArrayList<>(Arrays.asList(vodka,orange));

    }

    /**
     * Permet de parser les données
     * @param msg
     */
    public void MiseAJour(String msg, Context ctxt){
        //Contient id_ingredient, id_bec, nom_ingrédient
        Log.d("Recette", "debut maj");
        String[] listeIngredients = msg.split("/");
        ings = new ArrayList<>();
        for (String listeIngredient : listeIngredients) {
            String[] infoCocktail = listeIngredient.split(",");
            int bec;
            if (infoCocktail[1].equals("None")){
                bec = -1;
            }
            else{
                bec =  Integer.parseInt(infoCocktail[1]);
            }
            ings.add(new IngredientItem(infoCocktail[2], 0,bec, Integer.parseInt(infoCocktail[0])));


        }
        //Pour mettre à jour si elle existe déjà la page de la liste des ingrédients
        Intent signalIntent = new Intent("Update Ingredients");
        ctxt.sendBroadcast(signalIntent);
    }

    public void deleteIngredient(ArrayList<IngredientItem> toDelete){
        String msg = "";
        for (IngredientItem ingredientItem: toDelete){
            msg+=ingredientItem.id+"/";
        }
        msg = msg.substring(0, msg.length() - 1);
        Log.d("A supprimer", msg);
        DeviceControl dc = DeviceControl.getInstance();
        dc.writeOnDeleteIngredient(msg);

    }

    /**
     * Permet de trouver l'ingrédient correspondant au paramètre bec
     * @param bec = int
     * @return ing = l'ingrédient correspondant au bec
     */
    public IngredientItem findIngWithBec(int bec){
        for (IngredientItem ing: ings){
            if (ing.bec==bec){
                return ing;
            }
        }
        return null;
    }

    /**
     * Envoie la liste des ingrédients en fonction de leur bec à la machine
     */
    public void sendMaj(){
        // bec, id ingredient, quantité, "none" si non quantite
        String msg = "";
        for (int i=0; i<6; i++) {
            IngredientItem ing = findIngWithBec(i);
            if (ing!=null){
                msg+= ing.bec+","+ing.id+","+ing.quantity+"/";
            }
            else{
                msg+=i+",None,None/";
            }
        }
        msg = msg.substring(0, msg.length()-1);
        DeviceControl dc = DeviceControl.getInstance();
        dc.writeOnMajIngredientsCharacteristic(msg);

    }

    /**
     * Permet de renvoyer la liste des ingrédients à n'importe quel endroit du code
     * @return
     */
    public static Ingredients getInstance(){
        if (instance==null)
            new Ingredients();
            Log.d("aya","Les recettes n'ont pas été initialisées");
        return instance;
    }
}
