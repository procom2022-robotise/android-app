package com.example.robotise.model;


import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import java.util.ArrayList;

/**
 * Chaque cocktail à un nom, une image, une activité qui permet d'afficher plus de détails sur
 * ce cocktail, une liste des ingrédients et un booléen pour savoir si ce cocktail est favoris ou non
 */
public class CocktailItem  {
    public int id;
    public String name;
    public int image;
    public Intent activity;
    public ArrayList<IngredientItem> ingredients;
    public boolean favorite;


    /**
     * Constructeur de cocktailItem
     * @param id : id du cocktail
     * @param name : nom du cocktail
     * @param image : image du cocktail
     * @param activity : activité nécéssaire pour l'affichage des toast
     * @param ingredients : liste des ingrédients du cocktail
     */
    public CocktailItem(int id, String name, int image, Intent activity, ArrayList<IngredientItem> ingredients) {
        this.name = name;
        this.image = image;
        this.activity = activity;
        this.ingredients=ingredients;
        this.favorite = false;
        this.id = id;
    }

    /**
     * Permet de configurer ce cocktail comme favori ou non
     * @param fav : boolean
     */
    public void setFavorite(boolean fav) {
        Log.d("CocktailItem", name + "est favori : " + fav);
        this.favorite = fav;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}


