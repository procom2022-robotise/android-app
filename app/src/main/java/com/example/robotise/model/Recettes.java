package com.example.robotise.model;

import android.text.Editable;
import android.util.Log;


import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Contient la liste des cocktails
 */
public class Recettes {
    private static Recettes instance;
    public ArrayList<CocktailItem> cocktailsItems;
    ArrayList<Integer> cocktailDefaultImages = new ArrayList<>(Arrays.asList(R.mipmap.sotb, R.mipmap.blue_lagoon, R.mipmap.red_cocktails_icone));



    /**
     * Créer des recettes par défaut
     */
    public Recettes(){
        instance = this;
        cocktailsItems = new ArrayList<>();
        ArrayList<IngredientItem> blueLagoonIngredients = new ArrayList<IngredientItem>();
        ArrayList<IngredientItem> sotbIngredients = new ArrayList<IngredientItem>();
        blueLagoonIngredients.add(new IngredientItem("Vodka", 8,0));
        blueLagoonIngredients.add(new IngredientItem("Curacao", 6,1));
        blueLagoonIngredients.add(new IngredientItem("Jus de citron", 10,2));

        sotbIngredients.add(new IngredientItem("Vodka", 3,0));
        sotbIngredients.add(new IngredientItem("Ananas", 6,3));
        sotbIngredients.add(new IngredientItem("Cramberry", 10,4));

        CocktailItem sotbCocktail = new CocktailItem(0, "SOTB", R.mipmap.sotb, null, sotbIngredients);
        CocktailItem blueLagoonCocktail = new CocktailItem(1, "BLUELAGOON", R.mipmap.blue_lagoon, null, blueLagoonIngredients);
        cocktailsItems.add(sotbCocktail);
        cocktailsItems.add(blueLagoonCocktail);

    }

    public CocktailItem getCocktailWithId(int id){
        for (CocktailItem ci: cocktailsItems){
            if (ci.id == id){
                return ci;
            }
        }
        return null;
    }

    /**
     * Permet de parser les données
     * @param msg
     */
    public void MiseAJour(String msg){
        //"0,SOTB,0,vodka, quantity/"
        Log.d("Recette", "debut maj");
        String[] listeCocktails = msg.split("/");
        cocktailsItems = new ArrayList<>();
        for (String listeCocktail : listeCocktails) {
            String[] infoCocktail = listeCocktail.split(",");
            int id_cocktail = Integer.parseInt(infoCocktail[0]);
            CocktailItem cocktailItem = getCocktailWithId(id_cocktail);
            if (cocktailItem!=null) {
                //id, nomcocktail, nm ingredient , quantité
                IngredientItem ii = new IngredientItem(infoCocktail[3], Integer.parseInt(infoCocktail[4]), Integer.parseInt(infoCocktail[2]));
                cocktailItem.ingredients.add(ii);
            } else {
                ArrayList<IngredientItem> cocktailIngredient = new ArrayList<IngredientItem>();
                cocktailIngredient.add(new IngredientItem(infoCocktail[3], Integer.parseInt(infoCocktail[4]), Integer.parseInt(infoCocktail[2])));
                int image_cocktail;
                switch (infoCocktail[1]) {
                    case "sex on the beach":
                        image_cocktail = R.mipmap.sotb;
                        break;
                    case "mojito":
                        image_cocktail = R.mipmap.mojito;
                        break;
                    case "blue lagoon":
                        image_cocktail = R.mipmap.blue_lagoon;
                        break;
                    case "planteur":
                        image_cocktail = R.mipmap.red_cocktails_icone;
                        break;
                    case "violet":
                        image_cocktail = R.color.purple_500;
                        break;
                    case "rouge":
                        image_cocktail = R.color.rouge;
                        break;
                    case "orange":
                        image_cocktail = R.color.orange;
                        break;
                    case "bleu":
                        image_cocktail = R.color.bleu;
                        break;
                    case "vert":
                        image_cocktail = R.color.vert;
                        break;
                    case "jaune":
                        image_cocktail = R.color.jaune;
                        break;
                    default:
                        int index_random = (int) (Math.random() * cocktailDefaultImages.size());
                        image_cocktail = cocktailDefaultImages.get(index_random);
                        break;
                }



                cocktailItem = new CocktailItem(id_cocktail, infoCocktail[1], image_cocktail, null, cocktailIngredient);
                cocktailsItems.add(cocktailItem);
            }

        }
        cocktailsItems.add(new CocktailItem(-1, "Random Cocktail", R.mipmap.random, null, new ArrayList<>()));


    }

    public void sendAjoutCocktail(Editable nom, ArrayList<IngredientItem> listIng){
        String msg = nom.toString() + "/";
        for (IngredientItem i: listIng){
            if (i.quantity>0){
                msg+=i.id+","+i.quantity+"/";
            }

        }
        msg = msg.substring(0, msg.length() - 1);
        Log.d("Ajouter Recette", msg);
        DeviceControl dc = DeviceControl.getInstance();
        dc.writeOnAddCocktail(msg);

    }


    public void deleteCocktail(ArrayList<CocktailItem> cocktailsToDelete){
        String msg = "";
        for (CocktailItem cocktailItem: cocktailsToDelete){
            msg+=cocktailItem.id+"/";
        }
        msg = msg.substring(0, msg.length() - 1);
        Log.d("A supprimer", msg);
        DeviceControl dc = DeviceControl.getInstance();
        dc.writeOnDeleteCocktail(msg);
    }

    public void sendOrder(int id, ArrayList<IngredientItem> ings){
        //id_recettes, id_ing, quantity
        String msg = "";
        for (IngredientItem ing: ings){
            msg+=""+id+","+ing.id+","+ing.quantity+"/";
        }
        msg = msg.substring(0, msg.length() - 1);

        Log.d("order", msg);
        DeviceControl dc = DeviceControl.getInstance();
        dc.writeOnCommanderCocktailCharacteristic(msg);
    }


    public static Recettes getInstance(){
        if (instance==null)
            new Recettes();
        return instance;
    }
}
