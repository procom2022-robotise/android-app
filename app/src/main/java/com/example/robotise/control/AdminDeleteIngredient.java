package com.example.robotise.control;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;
import com.example.robotise.model.IngredientItem;
import com.example.robotise.model.Ingredients;

import java.util.ArrayList;

/**
 * Cette activité permet de supprimer un ingrédient et de communiquer avec le raspberry
 */
public class AdminDeleteIngredient extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.admin_delete_ingredient);

        ListView lv = findViewById(R.id.listViewDeleteIngredients);
        ArrayAdapter<IngredientItem> ingredientAdapter =
                new ArrayAdapter<IngredientItem>(AdminDeleteIngredient.this,  android.R.layout.simple_list_item_multiple_choice, Ingredients.getInstance().ings);
        lv.setAdapter(ingredientAdapter);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lv.setItemsCanFocus(false);


        Button deleteIngredient = findViewById(R.id.admin_supprimer_ingredient);

        deleteIngredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<IngredientItem> ingredientsChecked = new ArrayList<>();
                SparseBooleanArray checked = lv.getCheckedItemPositions();
                for (int i = 0; i < lv.getCount(); i++)
                    if (checked.get(i)) {
                        IngredientItem item = (IngredientItem) lv.getItemAtPosition(i);
                        ingredientsChecked.add(item);
                        /* do whatever you want with the checked item */
                    }
                Ingredients.getInstance().deleteIngredient(ingredientsChecked);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                DeviceControl.getInstance().listenToBDRecettesCharacteristic();
                finish();
            }
        });

    }

}
