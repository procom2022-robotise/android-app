package com.example.robotise.control;

import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.bluehomestudio.luckywheel.LuckyWheel;
import com.bluehomestudio.luckywheel.OnLuckyWheelReachTheTarget;
import com.bluehomestudio.luckywheel.WheelItem;
import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;
import com.example.robotise.model.IngredientItem;
import com.example.robotise.model.Ingredients;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Cette activité est un bonus à l'application. Il permet de faire tourner une roue qui choisira des ingrédients aléatoirement pour faire un cocktail
 * Il est basé sur le github com.github.mmoamenn:LuckyWheel_Android:0.3.0.
 */
public class WheelActivity extends AppCompatActivity {

    public Button mCommander;
    public Button mRecommencer;
    public TextView mIngredient1;
    public TextView mIngredient2;
    public TextView mIngredient3;
    public ArrayList<TextView> quantites;
    public int actualTarget;
    public ArrayList<IngredientItem> toOrder;
    public int quantiteRestante = 25;
    
    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wheel_activity);
        LuckyWheel lw = (LuckyWheel) findViewById(R.id.lucky_wheel);
        lw.setOutlineSpotShadowColor(ResourcesCompat.getColor(getResources(), R.color.purple_700, null));
        lw.setOutlineAmbientShadowColor(ResourcesCompat.getColor(getResources(), R.color.purple_700, null));
        lw.setDrawingCacheBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.purple_700, null));
        ArrayList<WheelItem> wheelItems = new ArrayList<>();
        int purple200 = ResourcesCompat.getColor(getResources(), R.color.purple_200, null);
        int purple500 = ResourcesCompat.getColor(getResources(), R.color.purple_500, null);
        ArrayList<Integer> colors = new ArrayList<Integer>(Arrays.asList(purple200, purple500));

        ArrayList<IngredientItem> ings = new ArrayList<>();
        for (IngredientItem ingredientItem:Ingredients.getInstance().ings){
            if (ingredientItem.bec>0){
                ings.add(ingredientItem);
            }
        }

        for(int i=0;i< ings.size();i++){
            WheelItem wi = new WheelItem(colors.get(i%2),
                    BitmapFactory.decodeResource(getResources(), com.bluehomestudio.luckywheel.R.drawable.ic_action_name), ings.get(i).name);
            wheelItems.add(wi);
        }
        
        mCommander = findViewById(R.id.wheel_activity_button_commander);
        mCommander.setEnabled(false);
        mCommander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "";
                String affichage = "Vous avez commander : ";

                for(int i=0; i<3; i++){
                    int quantiteIngredient;
                    IngredientItem ingredientItem = toOrder.get(i);
                    quantiteIngredient = Integer.parseInt(quantites.get(i).getText().toString());
                    msg+="-1,"+ingredientItem.id+","+quantiteIngredient+"/";
                    affichage+=quantiteIngredient+"cl de "+ingredientItem.name+", ";
                }
                msg = msg.substring(0, msg.length()-1);

                DeviceControl.getInstance().writeOnCommanderCocktailCharacteristic(msg);

                Toast.makeText(getApplicationContext(), affichage, Toast.LENGTH_LONG).show();
            }
        });
        mRecommencer=findViewById(R.id.wheel_activity_button_recommencer);
        mRecommencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recommencer();
            }
        });
        mIngredient1 = findViewById(R.id.ingredient1);
        mIngredient2 = findViewById(R.id.ingredient2);
        mIngredient3 = findViewById(R.id.ingredient3);

        quantites=new ArrayList<>();

        quantites.add(findViewById(R.id.quantite1));
        quantites.add(findViewById(R.id.quantite2));
        quantites.add(findViewById(R.id.quantite3));
        
        toOrder=new ArrayList<>();
        lw.addWheelItems(wheelItems);
        actualTarget = (int)(Math.random()*(ings.size())+1);
        lw.setTarget(actualTarget);

        
        lw.setLuckyWheelReachTheTarget(new OnLuckyWheelReachTheTarget() {
            @Override
            public void onReachTarget() {
                String nouvelIngredient = wheelItems.get(actualTarget-1).text;
                if (mIngredient1.getText()==""){
                    mIngredient1.setText(nouvelIngredient);
                    int quantiteIngredient = randomQuantity(quantiteRestante-6);
                    quantiteRestante-=quantiteIngredient;
                    quantites.get(0).setText(""+quantiteIngredient);
                }
                else if (mIngredient2.getText()==""){
                    mIngredient2.setText(nouvelIngredient);
                    int quantiteIngredient = randomQuantity(quantiteRestante-3);
                    quantiteRestante-=quantiteIngredient;
                    quantites.get(1).setText(""+quantiteIngredient);
                }
                else if (mIngredient3.getText()==""){
                    mIngredient3.setText(nouvelIngredient);
                    mCommander.setEnabled(true);
                    int quantiteIngredient = quantiteRestante;
                    quantites.get(2).setText(""+quantiteIngredient);
                }
                else{
                    recommencer();
                    mIngredient1.setText(nouvelIngredient);
                    int quantiteIngredient = randomQuantity(quantiteRestante-6);
                    quantiteRestante-=quantiteIngredient;
                    quantites.get(0).setText(""+quantiteIngredient);
                }
                toOrder.add(ings.get(actualTarget-1));
                
                actualTarget = (int)(Math.random()*(ings.size())+1);
                lw.setTarget(actualTarget);
            }


        });
    }

    public int randomQuantity(int quantite){
        return 3+(int) Math.floor(Math.random()*(quantite-3));
    }
    
    public void recommencer(){
        mIngredient1.setText("");
        mIngredient2.setText("");
        mIngredient3.setText("");
        quantites.get(0).setText("");
        quantites.get(1).setText("");
        quantites.get(2).setText("");
        mCommander.setEnabled(false);
        quantiteRestante = 25;
        toOrder = new ArrayList<>();
        
    }

}
