package com.example.robotise.control;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.robotise.R;
import com.example.robotise.list.IngredientArrayAdapter;
import com.example.robotise.model.IngredientItem;
import com.example.robotise.model.Ingredients;

import java.util.ArrayList;

/**
 * Cette activité permet de mettre à jour les quantités et les becs correspondants aux ingrédients mis dans la machine
 */
public class AdminMajQuantityActivity extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // A la création on relie chaque éléments à l'élément correspondant dans le layout
        ArrayList<IngredientItem> ing = Ingredients.getInstance().ings;

        setContentView(R.layout.admin_maj_quantite);
        //int ir = (int) b.get("ressource");



        ListView lv =findViewById(R.id.majListViewQuantité);
        Button majBD = findViewById(R.id.majBDQuantité);

        majBD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Ingredients.getInstance().sendMaj();
                finish();
            }
        });



        // Pour le test
        for (IngredientItem i: ing){
            System.out.println("ici il y a du ingredients items");
            System.out.println(i.name);
        }
        fillListViewIngredients(lv, ing);


        /*
        mValider = findViewById(R.id.retour);
        mValider.setOnClickListener(new View.OnClickListener(){
                                       public void onClick(View view) {
                                           finish();
                                       }

                                   }
        );*/

    }


    /**
     * Permet de remplir la liste des ingrédients
     * @param lv : liste view
     * @param ing : liste des ingrédients
     */
    public void fillListViewIngredients(ListView lv, ArrayList<IngredientItem> ing){
        IngredientArrayAdapter ingredientAdapter =
                new IngredientArrayAdapter( AdminMajQuantityActivity.this, 0, ing, IngredientArrayAdapter.Modes.AvecBecs);
        lv.setAdapter(ingredientAdapter);
    }


}
