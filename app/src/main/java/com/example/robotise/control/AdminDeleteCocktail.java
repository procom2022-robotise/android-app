package com.example.robotise.control;

import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;
import com.example.robotise.model.CocktailItem;
import com.example.robotise.model.Recettes;

import java.util.ArrayList;


/**
 * Cette activité permet de supprimer un cocktail et de communiquer avec le raspberry
 */
public class AdminDeleteCocktail extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.admin_delete_cocktail);

        ListView lv = findViewById(R.id.supprimerCocktailListView);
        ArrayAdapter<CocktailItem> ingredientAdapter =
                new ArrayAdapter<CocktailItem>(AdminDeleteCocktail.this,  android.R.layout.simple_list_item_multiple_choice, Recettes.getInstance().cocktailsItems);
        lv.setAdapter(ingredientAdapter);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lv.setItemsCanFocus(false);


        Button deleteCocktails = findViewById(R.id.delete_cocktail);

        deleteCocktails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CocktailItem> cocktailsChecked = new ArrayList<>();
                SparseBooleanArray checked = lv.getCheckedItemPositions();
                for (int i = 0; i < lv.getCount(); i++)
                    if (checked.get(i)) {
                        CocktailItem item = (CocktailItem) lv.getItemAtPosition(i);
                        cocktailsChecked.add(item);
                        /* do whatever you want with the checked item */
                    }
                Recettes.getInstance().deleteCocktail(cocktailsChecked);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                DeviceControl.getInstance().listenToBDRecettesCharacteristic();
                finish();
            }
        });

    }

}
