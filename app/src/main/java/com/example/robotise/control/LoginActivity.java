package com.example.robotise.control;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.robotise.R;
import com.example.robotise.model.Recettes;

/**
 * Activité permettant de se connecter à l'application après la configuration bluetooth
 */
public class LoginActivity extends Activity {
    // Deux radio button permettant de choisir le mode utilisateur ou administrateur
    RadioButton mUtilisateur;
    RadioButton mAdministrateur;
    RadioButton mRandomWheel;
    // Permet d'éditer le mdp
    EditText mMdp;
    //Permet de valider le mode et le mdp
    Button mValider;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("hello");


        // On relit le boutton valider mot de passe à son action
        setContentView(R.layout.login_activity);
        mValider = findViewById(R.id.buttonValiderFirst);
        //Selon le mode on démarre l'activité qui correspond
        mValider.setOnClickListener(view -> {
            boolean enable = true;
            if (mAdministrateur.isChecked() && !mMdp.getText().toString().equals("hello")){
                enable=false;
                Toast.makeText(getApplicationContext(),"Wrong password",Toast.LENGTH_LONG).show();
            }
            mMdp.setText("");
            if (mUtilisateur.isChecked()){
                Intent i = new Intent(getApplicationContext(), ListCocktailsActivity.class);
                startActivity(i);
            }
            else if (enable){
                Intent i = new Intent(getApplicationContext(), AdminActivity.class);
                startActivity(i);
            }
        }
        );
        mMdp = findViewById(R.id.mdp);
        mUtilisateur = findViewById(R.id.utilisateur);
        // Si le mode utilisateur est sélectionner, pas besoin de mdp
        mUtilisateur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMdp.setVisibility(View.GONE);
            }
        });
        mAdministrateur = findViewById(R.id.administrateur);
        // Si le mode administrateur est sélectionné, on a besoin d'un mdp
        mAdministrateur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMdp.setVisibility(View.VISIBLE);
            }
        });
    }
}

