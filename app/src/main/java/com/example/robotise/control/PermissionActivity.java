package com.example.robotise.control;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;
import com.example.robotise.ble.DeviceScanActivity;

import java.util.ArrayList;

/**
 * Cette activité permet de demander les permissions requises pour accéder à l'application
 */
public class PermissionActivity extends AppCompatActivity {

    private static int idx = 0;
    private static ArrayList<String> permissions;
    @RequiresApi(api = Build.VERSION_CODES.S)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ble_scan);
        permissions = new ArrayList<>();
        permissions.add(Manifest.permission.BLUETOOTH_ADVERTISE);
        permissions.add(Manifest.permission.BLUETOOTH);
        permissions.add(Manifest.permission.BLUETOOTH_SCAN);
        permissions.add(Manifest.permission.BLUETOOTH_ADMIN);
        permissions.add(Manifest.permission.BLUETOOTH_CONNECT);
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        askNextPermission();
    }


    @RequiresApi(api = Build.VERSION_CODES.S)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        // Checking whether user granted the permission or not.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // Showing the toast message
            Toast.makeText(PermissionActivity.this, " Permission  Granted", Toast.LENGTH_SHORT).show();
            idx++;
        } else {
            Toast.makeText(PermissionActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();

        }
        askNextPermission();

    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    public void askNextPermission(){
        Log.d("called", ""+idx+" "+permissions.size());
        if (idx>=permissions.size()){
            Intent i = new Intent(getApplicationContext(),DeviceScanActivity.class);
            startActivity(i);
        }
        else{
            checkPermission(permissions.get(idx), 1);
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    public void checkPermission(String permission, int requestCode)
    {
        // Checking if permission is not granted
        if (this.checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED) {
            this.requestPermissions(new String[] { permission }, requestCode);
        }
        else {
            Toast.makeText(PermissionActivity.this, "Permission already granted", Toast.LENGTH_SHORT).show();
            idx++;
            askNextPermission();
        }
    }
}
