package com.example.robotise.control;

import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.bluehomestudio.luckywheel.LuckyWheel;
import com.bluehomestudio.luckywheel.OnLuckyWheelReachTheTarget;
import com.bluehomestudio.luckywheel.WheelItem;
import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;
import com.example.robotise.model.CocktailItem;
import com.example.robotise.model.IngredientItem;
import com.example.robotise.model.Ingredients;
import com.example.robotise.model.Recettes;

import java.util.ArrayList;
import java.util.Arrays;
/**
 * Cette activité est un bonus à l'application. Elle permet de faire tourner une roue qui choisira une couleur aléatoirement
 * Elle a été créé juste pour la démo du forum où l'on utilisait des colorants à la place des jus
 * Il est basé sur le github com.github.mmoamenn:LuckyWheel_Android:0.3.0.
 */
public class WheelActivityColor extends AppCompatActivity {

    public Button mCommander;
    public Button mRecommencer;
    public TextView mIngredient1;
    public int actualTarget;
    public ArrayList<CocktailItem> toOrder;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wheel_activity);
        LuckyWheel lw = (LuckyWheel) findViewById(R.id.lucky_wheel);
        lw.setOutlineSpotShadowColor(ResourcesCompat.getColor(getResources(), R.color.purple_700, null));
        lw.setOutlineAmbientShadowColor(ResourcesCompat.getColor(getResources(), R.color.purple_700, null));
        lw.setDrawingCacheBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.purple_700, null));
        ArrayList<WheelItem> wheelItems = new ArrayList<>();
        int purple200 = ResourcesCompat.getColor(getResources(), R.color.purple_200, null);
        int purple500 = ResourcesCompat.getColor(getResources(), R.color.purple_500, null);
        ArrayList<Integer> colors = new ArrayList<Integer>(Arrays.asList(purple200, purple500));

        ArrayList<CocktailItem> ci = Recettes.getInstance().cocktailsItems;
        for(CocktailItem cocktailItem: ci){
            int color_item;
            switch (cocktailItem.name){
                case "violet":
                    color_item = purple500;
                    break;
                case "rouge":
                    color_item = ResourcesCompat.getColor(getResources(), R.color.rouge, null);
                    break;
                case "orange":
                    color_item = ResourcesCompat.getColor(getResources(), R.color.orange, null);;
                    break;
                case "bleu":
                    color_item = ResourcesCompat.getColor(getResources(), R.color.bleu, null);;
                    break;
                case "vert":
                    color_item = ResourcesCompat.getColor(getResources(), R.color.vert, null);;
                    break;
                case "jaune":
                    color_item = ResourcesCompat.getColor(getResources(), R.color.jaune, null);;
                    break;
                default:
                    color_item=ResourcesCompat.getColor(getResources(), R.color.purple_500, null);;
                    break;
            }
            if (cocktailItem.name!="Random Cocktail"){
                WheelItem wi = new WheelItem(color_item,
                        BitmapFactory.decodeResource(getResources(), com.bluehomestudio.luckywheel.R.drawable.ic_action_name), cocktailItem.name);
                wheelItems.add(wi);
            }

        }

        mCommander = findViewById(R.id.wheel_activity_button_commander);
        mCommander.setEnabled(false);
        mCommander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "";
                String affichage = "Vous avez commander : ";

                CocktailItem cocktailItem = toOrder.get(0);
                for (IngredientItem ii: cocktailItem.ingredients){
                    msg+=""+cocktailItem.id+","+ii.id+","+ii.quantity+"/";
                    affichage+=ii.quantity+"cl de "+ii.name+", ";
                }




                msg = msg.substring(0, msg.length()-1);

                DeviceControl.getInstance().writeOnCommanderCocktailCharacteristic(msg);

                Toast.makeText(getApplicationContext(), affichage, Toast.LENGTH_LONG).show();
            }
        });
        mRecommencer=findViewById(R.id.wheel_activity_button_recommencer);
        mRecommencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recommencer();
            }
        });
        mIngredient1 = findViewById(R.id.ingredient1);



        toOrder=new ArrayList<>();
        lw.addWheelItems(wheelItems);
        actualTarget = (int)(Math.random()*(ci.size())+1);
        lw.setTarget(actualTarget);


        lw.setLuckyWheelReachTheTarget(new OnLuckyWheelReachTheTarget() {
            @Override
            public void onReachTarget() {
                String nouveauCocktail = wheelItems.get(actualTarget-1).text;
                if (mIngredient1.getText()==""){
                    mIngredient1.setText(nouveauCocktail);
                }
                else{
                    recommencer();
                    mIngredient1.setText(nouveauCocktail);
                }
                mCommander.setEnabled(true);
                toOrder.add(ci.get(actualTarget-1));

                actualTarget = (int)(Math.random()*(ci.size())+1);
                lw.setTarget(actualTarget);
            }


        });
    }


    public void recommencer(){
        mIngredient1.setText("");
        mCommander.setEnabled(false);
        toOrder = new ArrayList<>();

    }

}
