package com.example.robotise.control;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;
import com.example.robotise.list.IngredientArrayAdapter;
import com.example.robotise.model.IngredientItem;
import com.example.robotise.model.Ingredients;
import com.example.robotise.model.Recettes;

import java.util.ArrayList;

/**
 * Cette activité permet d'ajouter un cocktail et de communiquer avec le raspberry
 */
public class AdminAddCocktail extends Activity {

    ArrayList<IngredientItem> ing;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // A la création on relie chaque éléments à l'élément correspondant dans le layout
         ing= new ArrayList<>();

        setContentView(R.layout.admin_add_cocktail);
        //int ir = (int) b.get("ressource");


        EditText nomCocktail = findViewById(R.id.ajouter_cocktail_name);

        ListView lv =findViewById(R.id.ajoutCocktailListViewIngredients);

        // Pour le test
        for (IngredientItem i: Ingredients.getInstance().ings){
            ing.add(i.copy());
            System.out.println("ici il y a du ingredients items");
            System.out.println(i.name);
        }
        fillListViewIngredients(lv, ing);


        Button ajoutCocktailValider = findViewById(R.id.ajouter_cocktail);

        ajoutCocktailValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nomCocktail.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Veuillez ajouter un nom de cocktail", Toast.LENGTH_LONG).show();
                }
                else if (allZeroQuantity()){
                    Toast.makeText(getApplicationContext(), "Toutes les quantités sont nulles", Toast.LENGTH_LONG).show();
                }
                else{
                    Recettes.getInstance().sendAjoutCocktail(nomCocktail.getText(), ing);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    DeviceControl.getInstance().listenToBDRecettesCharacteristic();
                    finish();
                }

            }
        });

    }

    public boolean allZeroQuantity(){
        for (IngredientItem ingredientItem:ing){
            if (ingredientItem.quantity!=0){
                return false;
            }
        }
        return true;
    }


    /**
     * Permet de remplir la liste des ingrédients
     * @param lv : liste view
     * @param ing : liste des ingrédients
     */
    public void fillListViewIngredients(ListView lv, ArrayList<IngredientItem> ing){
        IngredientArrayAdapter ingredientAdapter =
                new IngredientArrayAdapter( AdminAddCocktail.this, 0, ing, IngredientArrayAdapter.Modes.QuantiteSimple);
        lv.setAdapter(ingredientAdapter);
    }


}
