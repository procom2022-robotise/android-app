package com.example.robotise.control;

import static com.example.robotise.list.IngredientArrayAdapter.Modes.QuantiteSimple;

import android.app.Activity;
import android.os.Bundle;
import android.transition.Transition;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.core.view.ViewCompat;

import com.example.robotise.R;
import com.example.robotise.list.IngredientArrayAdapter;
import com.example.robotise.model.IngredientItem;
import com.example.robotise.model.Recettes;

import java.util.ArrayList;

/**
 * Cette activité permet d'avoir plus de détail sur un cocktail spécifique et communique avec le raspberry
 */
public class PopUpCocktail extends Activity {
    private Button mRetour;
    // View name of the header image. Used for activity scene transitions
    public static final String VIEW_NAME_HEADER_IMAGE = "detail:header:image";
    public static final String VIEW_NAME_HEADER_NOM = "detail:header:button";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // A la création on relie chaque éléments à l'élément correspondant dans le layout
        Bundle b = getIntent().getExtras();
        String nomCocktail= (String) b.get("nom");
        int id = (Integer) b.get("id");
        setContentView(R.layout.info_cocktail);
        int ir = (int) b.get("ressource");
        ArrayList<IngredientItem> ing = (ArrayList<IngredientItem>) b.getSerializable("ingredients");

        System.out.println("hello");

        ImageView iv = findViewById(R.id.imageCocktail);
        iv.setImageResource(ir);

        ViewCompat.setTransitionName(iv, VIEW_NAME_HEADER_IMAGE);

        Button nom = findViewById(R.id.nomCocktail);
        nom.setText(nomCocktail);
        ViewCompat.setTransitionName(nom, VIEW_NAME_HEADER_NOM);


        ListView lv =findViewById(R.id.listViewIngredients);

        Button commander = findViewById(R.id.commanderCocktail);



        // Pour le test
        for (IngredientItem i: ing){
            System.out.println("ici il y a du ingredients items");
            System.out.println(i.name);
        }
        fillListViewIngredients(lv, ing);

        mRetour = findViewById(R.id.retour);
        mRetour.setOnClickListener(new View.OnClickListener(){
               public void onClick(View view) {
                   finish();
               }

           }
        );

        // Si le bouton commandé est activé on envoie le nom du cocktail au raspberry
        commander.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                ArrayList<IngredientItem> ings = ((IngredientArrayAdapter) lv.getAdapter()).items;

                Recettes.getInstance().sendOrder(id, ings);

            }
        });

    }


    /**
     * Permet de remplir la liste des ingrédients
     * @param lv : liste view
     * @param ing : liste des ingrédients
     */
    public void fillListViewIngredients(ListView lv, ArrayList<IngredientItem> ing){
        IngredientArrayAdapter ingredientAdapter =
                new IngredientArrayAdapter( PopUpCocktail.this, 0, ing, QuantiteSimple);
        lv.setAdapter(ingredientAdapter);
    }

    /**
     * Try and add a {@link Transition.TransitionListener} to the entering shared element
     * {@link Transition}. We do this so that we can load the full-size image after the transition
     * has completed.
     *
     * @return true if we were successful in adding a listener to the enter transition
     */

    private boolean addTransitionListener() {
        final Transition transition = getWindow().getSharedElementEnterTransition();

        if (transition != null) {
            // There is an entering shared element transition so add a listener to it
            transition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionEnd(Transition transition) {
                    // As the transition has ended, we can now load the full-size image
                    //loadFullSizeImage();

                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionStart(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionCancel(Transition transition) {
                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionPause(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionResume(Transition transition) {
                    // No-op
                }
            });
            return true;
        }

        // If we reach here then we have not added a listener
        return false;
    }


}
