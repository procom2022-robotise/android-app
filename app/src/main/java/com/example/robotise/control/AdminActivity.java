package com.example.robotise.control;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.robotise.R;
import com.example.robotise.ble.DeviceControl;

/**
 * Cette activité est utile pour l'administrateur
 * Elle permet à l'administrateur de choisir parmi toutes ces fonctionnalités
 */
public class AdminActivity extends Activity {
    Button mCommanderCocktail;
    Button mAjouterCocktail;
    Button mSupprimerCocktail;
    Button mMajMachine;
    Button mMajQuantite;
    Button mAjouterIngredients;
    Button mSupprimerIngredients;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_activity);

        // On récupère chaque bouton liés à l'activité administrateur et on leur attribue leur fonction

        mCommanderCocktail = findViewById(R.id.admin_choisir_cocktail);
        //Permet de retrouver la même activité qu'en tant qu'utilisateur
        mCommanderCocktail.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), ListCocktailsActivity.class);
                startActivity(i);
            }
        });

        // Pour l'instant envoie un message au raspberry : "Ajoute moi un cocktail"
        mAjouterCocktail = findViewById(R.id.admin_ajouter_cocktail);
        mAjouterCocktail.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AdminAddCocktail.class);
                startActivity(i);
            }
        });

        // Pour l'instant envoie un message au raspberry : "Nettoye la machine"
        mSupprimerCocktail = findViewById(R.id.admin_supprimer_cocktail);
        mSupprimerCocktail.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AdminDeleteCocktail.class);
                startActivity(i);
            }
        });

        // Demande au raspberry la liste des cocktails
        mMajMachine = findViewById(R.id.admin_maj_machine);
        mMajMachine.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Mettre à jour machine",Toast.LENGTH_LONG).show();
                DeviceControl dc = DeviceControl.getInstance();
                dc.listenToBDRecettesCharacteristic();


            }
        });

        // Démarre l'activité mettre à jour la liste des cocktails
        mAjouterIngredients = findViewById(R.id.admin_ajouter_ingrédients);
        mAjouterIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AdminAddIngredientActivity.class);
                startActivity(i);
            }
        });

        // Démarre l'activité mettre à jour la liste des cocktails
        mSupprimerIngredients = findViewById(R.id.admin_supprimer_ingrédients);
        mSupprimerIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AdminDeleteIngredient.class);
                startActivity(i);
            }
        });

        // Démarre l'activité mettre à jour la liste des quantités
        mMajQuantite = findViewById(R.id.admin_maj_quantite);
        mMajQuantite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AdminMajQuantityActivity.class);
                startActivity(i);
            }
        });
    }

    public void startActivity(Intent i) {
        ActivityOptions options =
                ActivityOptions.makeSceneTransitionAnimation(this);
        startActivity(i, options.toBundle());
    }
}


