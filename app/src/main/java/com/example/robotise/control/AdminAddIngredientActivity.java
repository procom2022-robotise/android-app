
package com.example.robotise.control;

        import android.app.Activity;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ListView;

        import com.example.robotise.R;
        import com.example.robotise.ble.DeviceControl;
        import com.example.robotise.list.IngredientArrayAdapter;
        import com.example.robotise.model.IngredientItem;
        import com.example.robotise.model.Ingredients;

        import java.util.ArrayList;

/**
 * Cette activité permet d'ajouter un ingrédient et de communiquer avec le raspberry
 */
public class AdminAddIngredientActivity extends Activity {
    private Button mValider;
    private EditText mEditName;
    private static ArrayList<IngredientItem> ing;
    private ListView mListView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // A la création on relie chaque éléments à l'élément correspondant dans le layout

        DeviceControl dc = DeviceControl.getInstance();
        dc.listenToBDIngredientsCharacteristic();
        Log.d("Listen", "send");

        setContentView(R.layout.admin_add_ingredient);
        //int ir = (int) b.get("ressource");
        ing = Ingredients.getInstance().ings;
        mListView =findViewById(R.id.majListViewIngredients);


        fillListViewIngredients(AdminAddIngredientActivity.this, mListView, ing);

        mEditName = findViewById(R.id.edit_text_ajout_liquide);

        Button btnAjout = findViewById(R.id.button_ajout_liquide);
        btnAjout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nom = mEditName.getText().toString();
                if (nom.isEmpty() ){
                    return;
                }
                //ing.add(new IngredientItem(nom, 0, -1));
                Log.d("Ajout", "fait");
                //fillListViewIngredients(AdminMajListIngredientActivity.this, mListView, ing);
                DeviceControl dc = DeviceControl.getInstance();
                dc.writeOnAjouterIngredientCharacteristic(nom);

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dc.listenToBDIngredientsCharacteristic();
                getApplicationContext().registerReceiver(broadcastReceiver, new IntentFilter("Update Ingredients"));
                //ici METTRE à JOUR LA LISTE DE NOS INGREDIENTS
            }
        });
        mValider = findViewById(R.id.majsuite);
        mValider.setOnClickListener(new View.OnClickListener(){
               public void onClick(View view) {
                   finish();
               }

           }
        );

    }

    public void receiveNewData(){
        //Contient id_ingredient, id_bec, nom_ingrédient
        fillListViewIngredients(AdminAddIngredientActivity.this, mListView, Ingredients.getInstance().ings);
        IngredientArrayAdapter adapter = (IngredientArrayAdapter)(mListView.getAdapter());
        adapter.notifyDataSetChanged();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("Receive something", "receive");
            receiveNewData();
        }
    };

    @Override
    public void onPause() {
        super.onPause();

        getApplicationContext().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        DeviceControl dc = DeviceControl.getInstance();
        dc.listenToBDIngredientsCharacteristic();
        getApplicationContext().registerReceiver(broadcastReceiver, new IntentFilter("Update Ingredients")); // the constant is to identify the type of signal, it can be any string you want
    }


    /**
     * Permet de remplir la liste des ingrédients
     * @param lv : liste view
     * @param ing : liste des ingrédients
     */
    public static void fillListViewIngredients(Context context, ListView lv, ArrayList<IngredientItem> ing){
        IngredientArrayAdapter ingredientAdapter =
                new IngredientArrayAdapter( context, 0, ing, IngredientArrayAdapter.Modes.AvecCheckList);
        lv.setAdapter(ingredientAdapter);
    }


}
