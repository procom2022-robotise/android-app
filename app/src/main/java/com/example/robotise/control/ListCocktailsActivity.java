package com.example.robotise.control;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.robotise.R;
import com.example.robotise.list.CocktailsArrayAdapter;
import com.example.robotise.list.CocktailsListView;
import com.example.robotise.model.CocktailItem;
import com.example.robotise.model.Recettes;
import com.google.android.material.appbar.CollapsingToolbarLayout;

/**
 * Permet d'afficher la liste des cocktails disponibles
 */
public class ListCocktailsActivity extends AppCompatActivity  implements
        AdapterView.OnItemSelectedListener{
    private final String[] itemsComboBox = {"nom", "ingrédient"};
    private Spinner spin;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //De base pour le haut de l'application
        com.example.robotise.databinding.ActivityScrollingBinding binding = com.example.robotise.databinding.ActivityScrollingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolBarLayout = binding.toolbarLayout;
        toolBarLayout.setTitle("Liste Cocktails");




        //Ici on récupère la liste des recettes
        Recettes recettes = Recettes.getInstance();



        //Pour le spinner qui permet de trier en fonction des ingrédients ou des noms de cocktails
        // On recherche le spinner dans le layout et on lui applique OnItemSelectedListener
        spin = (Spinner) findViewById(R.id.spinner_cocktail_choice);
        spin.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        //On créer l'adapteur qui va contenir ce spinner
        ArrayAdapter spinAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item,itemsComboBox);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(spinAdapter);

        // On attribue les éléments à ce qui correspond dans le layout
        CocktailsListView cocktailsListView= findViewById(R.id.cocktailsListView);
        SearchView searchtext = findViewById(R.id.search_cocktails);
        CheckBox cbFavorite = findViewById(R.id.checkboxFavorite);


        // On remplit la liste des cocktails
        cocktailsListView.fillListView(recettes, spin, cbFavorite);


        // Pour la partie recherche de cocktails:
        // Cette fonction est appelée lorsque l'on change la zone de texte qui nous permet de chercher un cocktail
        searchtext.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ArrayAdapter<CocktailItem> adapter = (ArrayAdapter<CocktailItem>) cocktailsListView.getAdapter();
                adapter.getFilter().filter(s);

                return false;
            }
        });

        // Cette fonction est appelé quand on clique sur la checkbox des favoris
        cbFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CocktailsArrayAdapter)cocktailsListView.getAdapter()).getFilter().filter(searchtext.getQuery().toString());
            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "settings", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.change_user) {
            Toast.makeText(getApplicationContext(), "change_users", Toast.LENGTH_SHORT).show();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }




    /**
     * Cette fonction est appelé quand l'élément principal du spinner est modifié (ingrédients ou noms)
     */
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("ListCocktails", ((TextView)spin.getSelectedView()).getText().toString());
        //Toast.makeText(getApplicationContext(),((TextView)spin.getSelectedView()).getText().toString() , Toast.LENGTH_LONG).show();
    }

    /**
     * Useless
     * @param adapterView
     */
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}