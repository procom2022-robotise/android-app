/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.robotise.ble;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.robotise.control.ListCocktailsActivity;
import com.example.robotise.model.Ingredients;
import com.example.robotise.model.Recettes;
import com.example.robotise.morpion.MorpionActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControl {
    private final static String TAG = TestDeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private static DeviceControl instance;
    private String mDeviceName;
    private String mDeviceAddress;
    private Context mContext;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    public BluetoothGattCharacteristic CommanderCocktailCharacterisitic;
    public BluetoothGattCharacteristic BDRecettesCharacterisitic;
    public BluetoothGattCharacteristic BDIngredientsCharacteristic;
    public BluetoothGattCharacteristic MajIngredientsCharacteristic;
    public BluetoothGattCharacteristic AjouterIngredientCharacteristic;
    public BluetoothGattCharacteristic AddCocktailCharacteristic;
    public BluetoothGattCharacteristic DeleteCocktailCharacteristic;
    public BluetoothGattCharacteristic DeleteIngredientCharacteristic;

    public boolean commandeEnCours = false;


    DeviceControl(Context c, String deviceName, String deviceAddress) {
        while (ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions((Activity) c, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }

        mDeviceName = deviceName;
        mDeviceAddress = deviceAddress;
        mContext = c;
        instance = this;


        Intent gattServiceIntent = new Intent(c, BluetoothLeService.class);
        // Code to manage Service lifecycle.
        // Automatically connects to the device upon successful start-up initialization.
        ServiceConnection mServiceConnection = new ServiceConnection() {

            @RequiresApi(api = Build.VERSION_CODES.S)
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder service) {
                mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
                if (!mBluetoothLeService.initialize()) {
                    Log.e(TAG, "Unable to initialize Bluetooth");
                    instance = null;
                }
                // Automatically connects to the device upon successful start-up initialization.
                boolean res = false;
                try {
                    res = mBluetoothLeService.connect(mDeviceAddress);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d("connect", "connect success:" + res);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.d("Service", "Disconnected");
                mBluetoothLeService.close();
                mBluetoothLeService = null;
            }
        };
        c.bindService(gattServiceIntent, mServiceConnection, AppCompatActivity.BIND_AUTO_CREATE);
        // Handles various events fired by the Service.
        // ACTION_GATT_CONNECTED: connected to a GATT server.
        // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
        // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
        // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
        //                        or notification operations.
        // Show all the supported services and characteristics on the user interface.
        BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();
                if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                    System.out.println("Connected");
                    mConnected = true;
                } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                    mConnected = false;
                    onError();

                } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                    // Show all the supported services and characteristics on the user interface.
                    addGattServices(mBluetoothLeService.getSupportedGattServices());
                } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                    receivedData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA),
                            intent.getStringExtra("UUID"));
                }
            }
        };
        c.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }

    private void onError(){
        Intent main_ = new Intent(mContext, DeviceScanActivity.class);
        main_.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(main_);
    }

    private void receivedData(String data, String uuid){
        Log.d("Data received:", data);
        //Toast.makeText(mContext,"Data received"+data,Toast.LENGTH_LONG).show();
        if (uuid.equals(BDRecettesCharacterisitic.getUuid().toString())){
            Recettes.getInstance().MiseAJour(data);
            mNotifyCharacteristic = null;
            mBluetoothLeService.setCharacteristicNotification(
                    BDRecettesCharacterisitic, false);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            listenToBDIngredientsCharacteristic();
        }
        else if (uuid.equals(BDIngredientsCharacteristic.getUuid().toString())){
            Ingredients.getInstance().MiseAJour(data, mContext);
            mNotifyCharacteristic = null;
            mBluetoothLeService.setCharacteristicNotification(
                    BDIngredientsCharacteristic, false);
        }

        else if (uuid.equals(CommanderCocktailCharacterisitic.getUuid().toString())){
            //Ingredients.getInstance().MiseAJour(data, mContext);

            mNotifyCharacteristic = null;
            mBluetoothLeService.setCharacteristicNotification(
                    CommanderCocktailCharacterisitic, false);
            Toast.makeText(mContext, "Votre cocktail est terminé", Toast.LENGTH_LONG).show();
            Log.d("Cocktail", "Bien Recu");
            MorpionActivity.getInstance().onCocktailFinish();
            commandeEnCours = false;
        }
    }

    /**
     * Permet de communiquer avec le raspberry pour ajouter un cocktail
     * @param data
     */
    public void writeOnAddCocktail(String data){
        if (AddCocktailCharacteristic!=null){
            AddCocktailCharacteristic.setValue(data);
            mBluetoothLeService.writeCharacteristic(AddCocktailCharacteristic);
        }
        else{
            Log.d("Characteristic", "AddCocktailCharacteristic is nuuuuuuuuuuuuull");
            onError();
        }
    }

    /**
     * Permet de communiquer avec le raspberry pour supprimer un cocktail
     * @param data
     */
    public void writeOnDeleteCocktail(String data){
        if (DeleteCocktailCharacteristic!=null){
            DeleteCocktailCharacteristic.setValue(data);
            mBluetoothLeService.writeCharacteristic(DeleteCocktailCharacteristic);
        }
        else{
            Log.d("Characteristic", "DeleteCocktailCharacteristic is nuuuuuuuuuuuuull");
            onError();
        }
    }

    /**
     * Permet de communiquer avec le raspberry pour supprimer un ingrédient
     * @param data
     */
    public void writeOnDeleteIngredient(String data){
        if (DeleteIngredientCharacteristic!=null){
            DeleteIngredientCharacteristic.setValue(data);
            mBluetoothLeService.writeCharacteristic(DeleteIngredientCharacteristic);
        }
        else{
            Log.d("Characteristic",  "DeleteIngredientCharacteristic is nuuuuuuuuuuuuull");
            onError();
        }
    }

    /**
     * Permet de communiquer pour permettre la mise à jour des ingrédients
     * @param data
     */
    public void writeOnMajIngredientsCharacteristic(String data){
        if (MajIngredientsCharacteristic !=null){
            MajIngredientsCharacteristic.setValue(data);
            boolean res =mBluetoothLeService.writeCharacteristic(MajIngredientsCharacteristic);
        }
        else{
            Log.d("Characteristic", "MajIngredientsCharacteristic is nuuuuuuuuuuuuull");
            onError();
        }
    }

    /**
     * Permet de communiquer pour permettre l'ajout d'un ingrédient
     * @param data
     */
    public void writeOnAjouterIngredientCharacteristic(String data){
        if (AjouterIngredientCharacteristic !=null){
            AjouterIngredientCharacteristic.setValue(data);
            boolean res =mBluetoothLeService.writeCharacteristic(AjouterIngredientCharacteristic);
        }
        else{
            Log.d("Characteristic", "AjouterIngredientCharacteristic is nuuuuuuuuuuuuull");
            onError();
        }
    }

    /**
     * Permet de commander un cocktail
     * @param data
     */
    public void writeOnCommanderCocktailCharacteristic(String data){

        if (CommanderCocktailCharacterisitic !=null && !commandeEnCours){
            mNotifyCharacteristic = CommanderCocktailCharacterisitic;
            mBluetoothLeService.setCharacteristicNotification(
                    CommanderCocktailCharacterisitic, true);

            commandeEnCours = true;
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            CommanderCocktailCharacterisitic.setValue(data);
            boolean res =mBluetoothLeService.writeCharacteristic(CommanderCocktailCharacterisitic);
            Log.d("Réponse au write", String.valueOf(res));

            Intent i = new Intent(mContext, MorpionActivity.class);
            mContext.startActivity(i);
        }
        else if(commandeEnCours){
            Toast.makeText(mContext,"Une commande est déjà en cours, veuillez patienter..", Toast.LENGTH_LONG).show();
        }
        else{
            Log.d("Characterisitic", "CommanderCocktailCharacteristic is nuuuuuuuuuuuuull");
            onError();
        }
    }

    /**
     * Demande au raspberry les informations sur les recettes
     */
    public void listenToBDRecettesCharacteristic() {
        Log.d("here", "INDICATION CHARACTERISTIC");
        if (BDRecettesCharacterisitic ==null){
            Log.d("Characteristic", "BDRecettesCharacteristic is nuuuuuuuull");
            onError();
        }
        else{
            mNotifyCharacteristic = BDRecettesCharacterisitic;
            mBluetoothLeService.setCharacteristicNotification(
                    BDRecettesCharacterisitic, true);
        }
    }

    /**
     * Demande au raspberry les informations sur  la liste des ingrédients
     */
    public void listenToBDIngredientsCharacteristic() {
        Log.d("here", "INDICATION CHARACTERISTIC");
        if (BDIngredientsCharacteristic ==null){
            Log.d("Characteristic", "BDIngredientsCharacteristic is nuuuuuuuull");
            onError();
        }
        else{
            mNotifyCharacteristic = BDIngredientsCharacteristic;
            mBluetoothLeService.setCharacteristicNotification(
                    BDIngredientsCharacteristic, true);
        }
    }

    /**
     * Retourne l'instance de la classe
     * @return
     */
    public static DeviceControl getInstance(){
        if (instance == null){
            Log.d("ERROR","No device connected");
        }
        return instance;
    }



    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void addGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid;
        String unknownServiceString = "Unknown service";
        String unknownCharaString = "Unknown characteristic";
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                Log.d("verif", uuid.toString());
                if (uuid.equals(SampleGattAttributes.COMMANDER_COCKTAIL_CHARACTERISTIC)){
                    CommanderCocktailCharacterisitic = gattCharacteristic;
                    Log.d("verif", "ouiiiiiiiiiiiiiii");
                }
                else if (uuid.equals(SampleGattAttributes.BD_RECETTES_CHARACTERISTIC) && BDRecettesCharacterisitic==null){
                    Log.d("Encore", "encore");
                    BDRecettesCharacterisitic = gattCharacteristic;
                    listenToBDRecettesCharacteristic();
                }
                else if(uuid.equals(SampleGattAttributes.BD_INGREDIENTS_CHARACTERISTIC)){
                    BDIngredientsCharacteristic = gattCharacteristic;
                }
                if(uuid.equals(SampleGattAttributes.MAJ_INGREDIENT_CHARACTERISTIC)){
                    MajIngredientsCharacteristic = gattCharacteristic;
                }
                if(uuid.equals(SampleGattAttributes.AJOUTER_INGREDIENT_CHARACTERISTIC)){
                    AjouterIngredientCharacteristic = gattCharacteristic;
                }
                if(uuid.equals(SampleGattAttributes.ADD_COCKTAIL_CHARACTERISTIC)){
                    AddCocktailCharacteristic = gattCharacteristic;
                }
                if(uuid.equals(SampleGattAttributes.DELETE_COCKTAIL_CHARACTERISTIC)){
                    DeleteCocktailCharacteristic = gattCharacteristic;
                }
                if(uuid.equals(SampleGattAttributes.DELETE_INGREDIENT_CHARACTERISTIC)){
                    DeleteIngredientCharacteristic = gattCharacteristic;
                }
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }


    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}