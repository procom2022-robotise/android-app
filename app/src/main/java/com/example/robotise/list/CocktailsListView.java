package com.example.robotise.list;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.Spinner;

import androidx.annotation.NonNull;

import com.example.robotise.control.WheelActivity;
import com.example.robotise.control.WheelActivityColor;
import com.example.robotise.model.CocktailItem;
import com.example.robotise.model.IngredientItem;
import com.example.robotise.control.PopUpCocktail;
import com.example.robotise.model.Recettes;

import java.util.ArrayList;

/**
 * View permettant d'afficher en grille la liste des cocktails. L'adapter correspondant est
 * le cocktailsArrayAdapter
 */
public class CocktailsListView extends GridView {

    public CocktailsListView(Context context) {
        super(context);
        setNumColumns(2);
    }
    public CocktailsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setNumColumns(2);
    }
    public CocktailsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setNumColumns(2);
    }

    /**
     * Permet d'avoir une listeView qui ne se scroll pas et donc qui ne se shrink pas
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightMeasureSpec_custom = MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec_custom);
        ViewGroup.LayoutParams params = getLayoutParams();
        params.height = getMeasuredHeight();
    }


    /**
     * Permet de remplir la liste des cocktail à partir de chacun de leur nom, image, description,
     *  et ingrédients
     * @param recettes
     * @param spin
     * @param cbFavorite
     */
    public void fillListView(@NonNull Recettes recettes, Spinner spin, CheckBox cbFavorite){
        // Un tableau avec tout les cocktails
        ArrayList<CocktailItem> arrayList=new ArrayList<>();
        for (CocktailItem cocktailItem: recettes.cocktailsItems)
        {
            // On créé une page avec la description, l'image, le nom et les ingrédients du cocktail
            Intent intent = new Intent(getContext(), PopUpCocktail.class);
            cocktailItem.activity = intent;
            if (cocktailItem.id == -1){
                cocktailItem.activity = new Intent(getContext(), WheelActivityColor.class);
            }
            intent.putExtra("ressource", cocktailItem.image);
            intent.putExtra("nom", cocktailItem.name);
            intent.putExtra("id", cocktailItem.id);
            intent.putExtra("ingredients", cocktailItem.ingredients);

            // On créé un item cocktail avec son nom, son image, sa page de description et ses ingrédients
            arrayList.add(cocktailItem);//On ajoute le cocktail à la liste des cocktails
        }

        // On créé l'adapter spécifique pour les cocktails
        CocktailsArrayAdapter cocktailAdapter =
                new CocktailsArrayAdapter(getContext(), 0, arrayList, spin, cbFavorite);
        setAdapter(cocktailAdapter);//sets the adapter for listView

        // Pour les tests
        System.out.println("yo");
        //System.out.println(getItemAtPosition(2).getClass());
    }
}