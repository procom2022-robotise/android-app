package com.example.robotise.list;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.example.robotise.R;
import com.example.robotise.control.PopUpCocktail;
import com.example.robotise.model.CocktailItem;
import com.example.robotise.model.IngredientItem;

import java.util.ArrayList;


/**
 * Cette classe permet d'afficher correctement un item cocktail avec son attribut favoris, son image, son nom
 */
public class CocktailsArrayAdapter extends ArrayAdapter<CocktailItem> implements Filterable {
    // Liste des cocktails
    ArrayList<CocktailItem> items;
    // Liste des cocktails triées
    ArrayList<CocktailItem> filteredItems;

    // Les éléments suivant permettent de filtrer en fonction du spin ou des favoris
    private ItemFilter mFilter = new ItemFilter();
    private Spinner spin;
    private CheckBox cbFavorite;

    /**
     * Constructeur
     * @param context :
     * @param resource :
     * @param objects :
     * @param spin :
     * @param cbFavorite :
     */
    public CocktailsArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CocktailItem> objects, Spinner spin, CheckBox cbFavorite) {
        super(context, resource, objects);
        this.items = objects;
        this.filteredItems = objects;
        this.spin = spin;
        this.cbFavorite= cbFavorite;

    }
    public int getCount() {
        return filteredItems.size();
    }

    public CocktailItem getItem(int position) {
        return filteredItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    static class ViewHolder {
        Button cocktailName;
        ImageView cocktailImage;
        ImageButton cocktailInfo;
        CheckBox cocktailFav;
    }

    @Override
    public View getView(int position,
                        View convertView,
                        ViewGroup parent) {
        CocktailItem currentCocktail = this.filteredItems.get(position);
        ViewHolder holder;
        if(convertView == null) {
            convertView = ((Activity)getContext()).getLayoutInflater()
                    .inflate(R.layout.cocktail_item, null, false);
            // Créé un viewholder et stocke toutes les références
            holder = new ViewHolder();
            holder.cocktailName =
                    (Button)convertView.findViewById(R.id.commanderCocktail);
            holder.cocktailImage =
                    (ImageView)convertView.findViewById(R.id.imageItemCocktail);
            holder.cocktailInfo =
                    (ImageButton)convertView.findViewById(R.id.infoItemCocktail);
            holder.cocktailFav =
                    (CheckBox) convertView.findViewById(R.id.favoriteStar);

            // On relie la data efficacement

            convertView.setTag(holder);


        }else {

            // On récupère le viewholder pour avoir un accès rapide à toutes les références
            holder = (ViewHolder) convertView.getTag();
        }


        holder.cocktailName.setText(currentCocktail.name);
        holder.cocktailImage.setImageResource(currentCocktail.image);

        holder.cocktailName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //((Activity)getContext()).startActivity(currentCocktail.activity);
                @SuppressWarnings("unchecked")
                ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        ((Activity)getContext()),

                        // Now we provide a list of Pair items which contain the view we can transitioning
                        // from, and the name of the view it is transitioning to, in the launched activity
                       new Pair<>( holder.cocktailImage, PopUpCocktail.VIEW_NAME_HEADER_IMAGE),
                        new Pair<>(holder.cocktailName, PopUpCocktail.VIEW_NAME_HEADER_NOM));

                // Now we can start the Activity, providing the activity options as a bundle
                ((Activity)getContext()).startActivity(currentCocktail.activity, activityOptions.toBundle());



            }

        });
        holder.cocktailInfo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Toast.makeText(((Activity)getContext()).getApplicationContext(),"Todo : change to favorites",Toast.LENGTH_LONG).show();
            }

        });

        holder.cocktailFav.setOnCheckedChangeListener(null);
        holder.cocktailFav.setChecked(currentCocktail.favorite);

        holder.cocktailFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                currentCocktail.setFavorite(isChecked);
            }
        });
        return convertView;

    }

    /**
     * Item permettant de filtrer correctement l'adaptateur
     */
    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<CocktailItem> list = items;

            int count = list.size();
            final ArrayList<CocktailItem> nlist = new ArrayList<CocktailItem>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {

                if (((TextView) spin.getSelectedView()).getText().toString().equals("nom")){
                    filterableString = list.get(i).name;

                    }
                else if (((TextView) spin.getSelectedView()).getText().toString().equals("ingrédient")){
                    filterableString="";
                    for (IngredientItem ing: list.get(i).ingredients){
                        filterableString+=ing.name;
                    }
                }
                else{
                    filterableString="";
                }

                System.out.println("yoman ca filtered"+filterableString);
                if (filterableString.toLowerCase().contains(filterString)) {
                    if (!cbFavorite.isChecked() || list.get(i).favorite){
                        nlist.add(list.get(i));
                    }
                    System.out.println("yoman c'est ajouté"+filterableString);



                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredItems = (ArrayList<CocktailItem>) results.values;
            notifyDataSetChanged();
        }

    }
}
