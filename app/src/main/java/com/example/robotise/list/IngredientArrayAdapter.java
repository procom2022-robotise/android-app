package com.example.robotise.list;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.robotise.R;
import com.example.robotise.model.IngredientItem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;



/**
 * Permet de faire apparaître la liste des ingrédient comme il le faut
 */
public class IngredientArrayAdapter extends ArrayAdapter<IngredientItem> {
    //Liste des ingrédients
    public ArrayList<IngredientItem> items;
    Modes mode; //Différents mode : QuantiteSimple, AvecCheckList, AvecBecs. Cela permet d'avoir différente manière d'afficher un ingrédient
    Context context;

    public IngredientArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<IngredientItem> objects, Modes mode) {
        super(context, resource, objects);
        this.items = objects;
        this.mode = mode;
        this.context = context;

    }
    public int getCount() {
        return items.size();
    }

    public IngredientItem getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    //Cette classe permet de garder en mémoire le nom de l'ingrédient, sa quantité et ses boutons
    // cela permet d'éviter d'aller les rechercher dans le layout à chaque fois
    static class ViewHolder {
        TextView ingredientName;
        EditText quantity;
        FloatingActionButton plus;
        FloatingActionButton minus;
        EditText nbBec;
        LinearLayout layoutQuantity;
        LinearLayout layoutBec;
    }

    public enum Modes{QuantiteSimple, AvecCheckList, AvecBecs}

    @Override
    public View getView(int position,
                        View convertView,
                        ViewGroup parent) {
        IngredientItem currentIngredient = items.get(position);
        ViewHolder holder;
        convertView = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.ingredient_item, null, false);

        // Créé un viewholder et stocke toutes les références
        holder = new ViewHolder();
        holder.ingredientName =
                (TextView) convertView.findViewById(R.id.ingredientName);
        holder.layoutQuantity =
                (LinearLayout) convertView.findViewById(R.id.ingredientLayoutQuantity);
        holder.quantity =
                (EditText) convertView.findViewById(R.id.ingredientQuantity);
        holder.plus =
                (FloatingActionButton) convertView.findViewById(R.id.ingredientPlus);
        holder.minus =
                (FloatingActionButton) convertView.findViewById(R.id.ingredientMinus);
        holder.nbBec = (EditText) convertView.findViewById(R.id.edit_text_bec);
        holder.layoutBec = (LinearLayout) convertView.findViewById(R.id.becLayout);

        // On relie la data efficacement

        convertView.setTag(holder);

        holder.ingredientName.setText(currentIngredient.name);
        holder.quantity.setText(""+currentIngredient.quantity);

        holder.quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text= editable.toString();
                if (!text.equals("")) {
                    currentIngredient.quantity = Integer.parseInt(text);

                }


            }
        });


        // En appuyant sur les boutons + et - on modifie la quantité actuelle de l'ingrédients dans le cocktails
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = Integer.parseInt(holder.quantity.getText().toString());
                q-=1;
                if (q>=0){
                    holder.quantity.setText(""+q);
                    currentIngredient.quantity = q;
                }
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = Integer.parseInt(holder.quantity.getText().toString());
                q+=1;
                holder.quantity.setText(""+q);
                currentIngredient.quantity = q;
            }
        });
        holder.layoutBec.setVisibility(View.GONE);
        holder.nbBec.setText(""+currentIngredient.bec);
        holder.nbBec.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            /**
             * Cette fonction est appelé après chaque modification du texte du nombre de brec
             * @param editable : texte qui a été modifié
             */
            @Override
            public void afterTextChanged(Editable editable) {
                String text= editable.toString();
                if (!text.equals("")) {
                    if (text.charAt(0) == '-') {
                        if (!text.substring(1).equals("")) {
                            currentIngredient.bec = -1 * Integer.parseInt(text.substring(1));
                        }

                    } else {
                        currentIngredient.bec = Integer.parseInt(text);
                    }
                }

            }
        });


        /**
         * On adapte la view au différent modes
         */
        if (this.mode==Modes.AvecCheckList){
            holder.layoutQuantity.setVisibility(View.GONE);
        }
        if (this.mode==Modes.AvecBecs){
            holder.layoutBec.setVisibility(View.VISIBLE);
            holder.quantity.setFocusable(true);
        }



        return convertView;

    }

}
