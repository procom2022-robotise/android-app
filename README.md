
Robotise Application
===================================



Introduction
------------
Robotise est un projet que nous avons commencer indépendemment de l'école en 2ème année et que nous avons choisi de développer en projet 3A. Il s'agit d'un robot distributeur de cocktail. 
Avec cette application, l'utilisateur peut choisir le cocktail qu'il désire et le robot réalise son cocktail quelques secondes plus tard.  
Pour que cela fonction, l'application doit communiquer avec le raspberry contrôlant Robotise. Nous avons choisi d'utiliser le protocole de communication Bluetooth Low Energy
Robotise est un robot qui permet de préparer des cocktails. 

Pré-Requis
----------
Pour compiler l'application vous avez besoin de :
 - Android SDK 31
Pour utiliser l'application, certaines permissions doivent obligatoirement être acceptées: 
 - L'accès à la position
 - L'accès au bluetooth

Différentes Fonctionnalités
--------------------------

Cette application a de multiple fonctionnalités:
 - Elle permet à l'utilisateur de choisir un cocktail à commander 
 - Elle permet à l'administrateur de mettre à jour les cocktails de l'application par rapport à la base de données 
 - Elle permet à l'administrateur de mettre à jour les ingrédients et leurs quantités dans la base de données
 - Elle permet à l'utilisateur d'avoir des cocktails favoris


Déroulement de l'application 
----------------------------

Dans un premier temps, l'utilisateur doit accepter deux permissions: l'accès au bluetooth et l'accès à la position (classe PermissionActivity).  

Ensuite, la configuration BLE est obligatoire. L'application nous montre une liste des appareils Bluetooth Low Energy.
A ce moment là il suffit de choisir l'appareil avec le nom ou 'robotise' ou 'rpi-gatt-server'  
  
<img src="app_documentation/1_Chercher_appareils.jpg" height="500"/>
  
Quand l'appareil est selectionné, un service est créé pour gérer le bluetooth du téléphone.
Si l'appareil se déconnecte du ble pour une quelconque raison, onServiceDisconnected sera appelé.
Une fois le service bluetooth activé, un autre service est créé (BluetoothLeService) dans le but de gérer 
la connection et la communication entre ses deux éléments. Ce service se connecte alors à l'appareil rapsberry, 
et récupère le server gatt du raspberry. L'application communique avec le service qui à son tour communique avec [Bluetooth LE API][2].
L'implémentation BLE a été très fortement inspirée d'un projet déjà existant [Bluetooth LE GATT][1]


Pour que cela fonctionne correctement il faut que toutes les charactéristiques suivantes soit présentes dans le service:
 - CommanderCocktailCharacterisitic : la charactéristique qui permet d'envoyer une commande.
 - BDRecettesCharacterisitic : la caractéristique qui permet de recevoir la liste des recettes
 - BDIngredientsCharacteristic :la caractéristique qui permet de recevoir la liste des ingrédients présent dans la base de données
 - MajIngredientsCharacterisitc : la caractéristique qui permet de mettre à jour dans la base de données, les ingrédients disponibles dans la machine.  
 - AddCocktailCharacteristic : la caractéristique qui permet d'ajouter un cocktail dans la base de données  
 - DeleteCocktailCharacteristic : la caractéristique qui permet de supprimer un cocktail dans la base de données
 - AjouterIngredientCharacteristic : la caractéristique qui permet d'ajouter un ingrédient dans la base de données
 - DeleteIngredientCharacteristic : la caractéristique qui permet de supprimer un ingrédient dans la base de données

Si la configuration du BLE est un succès alors nous pouvons passer à la suite de l'application. Nous avons donc dans 
un premier temps une page qui permet la connection en tant qu'utilisateur ou administrateur (LoginActivity).  
  
<img src="app_documentation/2_Login.jpg" height="500"/>
  
L'utilisateur n'a pour l'instant accès qu'à une seule fonctionnalité: commander un cocktail. En se connectant la fenêtre (ListCocktailsActivity) 
s'ouvre et ainsi l'utilisateur peut voir la liste des cocktails. Cette liste est contrôlé par la classe CocktailListView. Chaque cocktail est représenté par la classe: CocktailItem et son affichage est contrôlé par la classe CocktailsArrayAdapter. En cliquant sur un des cocktail, une page s'ouvre (PopUpCocktail), 
avec la composition du cocktail. Chaque ingrédient est représenté par la classe: IngredientItem et son affichage est contrôlé dans la classe IngredientArrayAdapter. L'utilisateur peut modifier les quantités de chaque ingrédient et ensuite appuyer sur le bouton commander. Il peut également 
choisir de prendre un cocktail random. Il s'agit de la classe WheelActivity. L'utilsateur peut faire tourner un roue 3 fois et à chaque fois un ingrédient sera désigné et une quantité aléatoire lui sera attribuée. Ce qui formera un cocktail aléatoire.
  
<img src="app_documentation/3a_Choisir_cocktail.jpg" height="500"/> <img src="app_documentation/3b_Commander_cocktail.jpg" height="500"/> <img src="app_documentation/4_Random_wheel.jpg" height="500"/>
  
Après avoir commander son cocktail, l'utilisateur a la possibilité de jouer en duo au morpion en attendant son cocktail. Dès que le cocktail est finit, l'utilisateur sera notifié.  
  
De son côté l'administrateur a besoin d'un mot de passe pour se connecter. Pour l'instant le mot de passe est hello et est codé en dur dans l'application. Après s'être connecté, la fenêtre AdminActivity s'ouvre
et l'administrateur a plusieurs fonctionnalités disponibles. Il peut choisir un cocktail à commander(de même que l'utilisateur), ajouter ou supprimer un cocktail, ajouter ou supprimer un ingrédient, mettre à jour les quantités de chaque ingrédients disponibles dans la machine
et récupérer la liste des cocktails de la machine.
  
<img src="app_documentation/5_Menu_admin" height="500"/>
  
Si l'administrateur choisi de récupérer la liste des cocktails alors nous appelons la fonctions "listenToBdRecettesCharacteristic" qui écoute la charactéristique correspondant à la
liste des recettes. Le script python, après avoir détecté un nouveau listener, renvoie la liste des recettes. ReceivedData dans DeviceControl est alors appelé et peut appelé la fonction correspondantes. Il met donc à
jour les recettes sur l'application. 
  
Si l'administrateur choisi l'option ajouter un ingrédient, alors une nouvelle fenêtre s'ouvre AdminAddIngredientActivity. La liste des ingrédients est chargé depuis la base de donnée. L'administrateur a ensuite la possibilité 
d'ajouter un ingrédient. L'ingrédient sera envoyé ensuite via ble dans la base de donnée du raspberry. Ensuite la liste des ingrédients est envoyé du raspberry jusqu'au téléphone.
  
<img src="app_documentation/8_Ajouter_ingrédient.jpg" height="500"/>
  
Si l'administrateur choisi l'option supprimer un ingrédient, alors une nouvelle fenêtre s'ouvre AdminDeleteIngredientActivity. La liste des ingrédients est chargé depuis la base de donnée. L'administrateur a ensuite la possibilité
de choisir un ingrédient à supprimer. L'ingrédient sera envoyé ensuite via ble dans la base de donnée du raspberry. Ensuite la liste des ingrédients est envoyé du raspberry jusqu'au téléphone.
  
<img src="app_documentation/9_Supprimer_ingrédient.jpg" height="500"/>
  
Pour ajouter ou supprimer un cocktail le principe est le même. Pour ajouter un cocktail, on ne peut le faire qu'à partir de la liste des ingrédients déjà présente dans la base de données
Si l'on veut un ingrédient qui n'est pas présent il faut d'abord l'ajouter.
  
<img src="app_documentation/6_Ajouter_cocktail.jpg" height="500"/> <img src="app_documentation/7_Supprimer_cocktail.jpg" height="500"/>
  
Si l'administrateur choisi l'option mettre à jour les quantités des ingrédients dans la machine, alors une nouvelle fenêtre s'ouvre AdminMajQuantiteActivity. Sur cette fenêtre nous pouvons mettre à jour, les quantités et le bec qui correspond à chaque ingrédient. Dès que
l'administrateur a fini de mettre à jour les ingrédients alors les nouvelles données sont envoyées à la base de données. 
  
<img src="app_documentation/10_MAJ.jpg" height="500"/>





[1]:https://github.com/android/connectivity-samples/tree/main/BluetoothLeGatt
[2]:https://developer.android.com/reference/android/bluetooth/BluetoothGatt.html

Structure
---------
```
Robotise    
├── app  
│   ├── libs  
│   ├── src  
│   │   ├── androidTest  
│   │   │   └── java  
│   │   │       └── com/example/robotise  
│   │   └── main  
│   │       ├── java  
│   │       │   └── com/example/robotise  
│   │       │       ├── ble  
│   │       │       ├── bluetooth  
│   │       │       ├── control  
│   │       │       ├── list  
│   │       │       └── model  
│   │       │       └── morpion  
│   │       └── res  
│   │           ├── drawable  
│   │           ├── layout  
│   │           ├── menu  
│   │           ├── mipmap  
│   │           └── values  
│   └── build.gradle  
├── build.gradle  
└── settings.gradle  
```



Screenshots
-------------

<img src="app_documentation/2_Login.jpg" height="500"/> 

Getting Started
---------------

Ce code utilise Gradle build system. Pour reproduire ce projet, utilisez "gradlew build"
ou utilisez "Import Projet sur Android studio".

Support
-------

- Contact: emeline.maugez@imt-atlantique.net
